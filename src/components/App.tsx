import * as React from 'react'

import IMasterProps from '@cyberforce/web-essentials/intf/react/IMasterProps'

export default class App extends React.Component<IMasterProps, {}> {

  public componentDidMount() {
    console.log('Yay!')
  }

  public render(): any {
    return this.props.children
  }

}
