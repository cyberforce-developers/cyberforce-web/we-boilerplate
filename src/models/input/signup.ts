import InputModel from '@cyberforce/web-essentials/models/input'
import IKeyAny from '@cyberforce/web-essentials/intf/IKeyAny'
import { Utils } from '@cyberforce/web-essentials/modules/utils'

export default class SignupInput extends InputModel {
  constructor() {
    super({
      name: 'signup',
      data: {
        forename: {
          _: '',
          v: 'user_firstname',
          exact: true
        },
        surname: {
          _: '',
          v: 'user_lastname',
          exact: true
        },
        age: {
          _: '',
          v: 'age',
          exact: true
        },
        email: {
          _: '',
          v: 'email'
        },
        password: {
          _: '',
          v: 'password'
        },
        password2: {
          _: '',
          v: 'passwordCheck'
        },
      }
    })
  }

  public forRequest(): IKeyAny {
    const data = {
      ...super.forRequest(),
      password: Utils.hash(this.getValue('password'))
    }
    delete data['password2']
    return data
  }
}