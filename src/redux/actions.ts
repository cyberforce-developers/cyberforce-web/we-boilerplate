enum Actions {
  StoreValue = 'storeValue'
}

export enum RequestActions {
  storeResponse = 'storeResponse',
  trigger = 'requestTrigger',
  error = 'requestError'
}

export enum FormActions {
  OnInputChange = 'OnFormInputChange',
  OnSubmit = 'OnFormSubmit',
  OnValidate = 'OnFormValidate',
  complete = 'formRequestComplete',
  trigger = 'formRequestTrigger',
  serverError = 'formRequestServerError'
}

export default Actions
