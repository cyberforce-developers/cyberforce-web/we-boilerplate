import * as React from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { Utils } from '@cyberforce/web-essentials/modules/utils'

import { find } from '../routes'
import * as indexActions from '../redux/pages/index/actions'
import * as requestActions from '../redux/enhancers/request/actions'

import { IIndexState } from '../redux/pages/index/state'
import LifeCycles from '@cyberforce/web-essentials/props/LifeCycles'
import RequestMethods from '@cyberforce/web-essentials/props/RequestMethods'

class Index extends React.Component<any, IIndexState> {

  render(): JSX.Element {
    const reqStatus = this.props.requests.hasOwnProperty('test') && this.props.requests.test.lifecycle
      ? this.props.requests['test'].lifecycle
      : undefined

    const data = this.props.requests.hasOwnProperty('test') && this.props.requests.test.lifecycle && this.props.requests.test.lifecycle === LifeCycles.completed
      ? this.props.requests['test'].data
      : undefined

    return (
      <div>
        <h1>Hello World</h1>
        <ul>
          <li><NavLink to={find('about') || "/"}>About</NavLink></li>
          <li><NavLink to={find('register') || "/"}>Register</NavLink></li>
        </ul>
        <h3>Name: <span id="name">{this.props.name}</span></h3>
        <button id="changeValue" onClick={() => this.props.setValue(Utils.ID({ pattern: '8.ABCDEF' }))}>Change state</button>
        <button id="sendRequest" onClick={() => this.props.sendRequest('test')}>Send request</button>
        <h3>Request: {reqStatus}</h3>
        <h3>Data: {JSON.stringify(data)}</h3>
      </div>
    )
  }
}

const stateToProps = (state: any) => {
  return {
    name: state.r_index.name,
    requests: state.r_request
  }
}

const dispatchToProps = (dispatch: any) => {
  return {
    setValue: (name: string) => dispatch(indexActions.setValue(name)),
    sendRequest: (name: string) => dispatch(requestActions.onSend(name, {
      url: '/jobs/example',
      method: RequestMethods.GET
    }))
  }
}


export default connect(stateToProps, dispatchToProps)(Index)
