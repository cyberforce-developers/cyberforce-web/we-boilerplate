const t = {
  title: "Other american translation",
  inputs: {
    name: {
      label: 'Name',
      validation: 'custom validation text for name'
    },
    surname: {
      label: 'Surname'
    },
    code: {
      label: 'Code'
    }
  },
  validation: {
    unknown: 'unknown error',
    given: 'given',
    expected: 'expected',
    tooLong: 'too long',
    tooShort: 'too short',
    mustMatch: 'must match',
    mustNotMatch: 'must not match',
    regex: 'does not match to the pattern',
    eitherOr: 'either one of the values need to be good'
  }
}

module.exports = t