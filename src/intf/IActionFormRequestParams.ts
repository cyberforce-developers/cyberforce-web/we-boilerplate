import IActionRequestParams from '@cyberforce/web-essentials/intf/actions/IActionRequestParams'

export default interface IActionRequestFormParams extends IActionRequestParams {
  onValidate: any
  validate?: boolean
  fields?: string[]
}