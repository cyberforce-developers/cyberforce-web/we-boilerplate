import * as ReactDOM from 'react-dom'
import { boot } from '@cyberforce/web-essentials/bootstrap'
import IKeyAny from '@cyberforce/web-essentials/intf/IKeyAny'
import Locale from '@cyberforce/web-essentials/modules/locale'
import Languages from '@cyberforce/web-essentials/props/Languages'

import routes from './routes'
import { current, config } from './config'
import global from './global'
import bootstrapComponent from './components/App'
const enGB = require('./locale/en-GB')
const enUS = require('./locale/en-US')

// Custom reducers (enhancers)
import r_forms from './redux/enhancers/forms/state'
import r_request from './redux/enhancers/request/state'

const enhancers: IKeyAny = {
  r_forms,
  r_request
}

const locale = new Locale()
locale.addTranslation(Languages.gb, enGB)
locale.addTranslation(Languages.us, enUS)
locale.setCurrentLanguage(Languages.gb) // determine what language is used by a user or put some switch here

ReactDOM.render(
  boot({ routes, global, config, current, enhancers, locale, bootstrapComponent }),
  document.getElementById('app') as HTMLElement
)
