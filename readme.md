Installation

To be updated. (including npm run preinstall)


General architecture

The web application relies on a local web server to expose the build to the outside works with a certain server-side settings set. Additionally it requires an API instance which is used to test the UI against.


Working locally

Testing first, BDD/TDD technique

You should write your tests first for any UI elements and their behaviour you are using. Web applications rely on end-end tests only which  


Implementation





NPM commands

Below you can find a list of all commands used across the web application. Please update this section upon adding a new command. Make sure you are reusing the existing commands before adding a new one.

`npm run local`
Compresses the web application watcher in local environment, observing all the changes within /src directory and SCSS files. Don't forget to run a local web server and local API in order to be able to fully work on it.

`npm run local:test:gui`
Runs the Cypress test window watching the application changes and styling. Don't forget to run a local web server and a local API in order to use it.

`npm run local:test:text`
Runs the Cypress test framework without the UI dashboard (text only) watching the application changes and styling. Don't forget to run a local web server and a local API in order to use it.

`npm run local:app`
Performs the compression of the web application with local settings and watches for changes. You can use `WEBAPP_COMPRESSION` variable to enable compression during the build. Make sure the web server also launches supporting compression. The compression is disabled for local environment by default.

`npm run local:server`
Launches the local instance of a web server that serves the builded application to the world. It includes a watcher. It is recommended to send it in a separate terminal.

`npm run local:build`
Builds a web application for local environment without watching.

`npm run local:styles`
Runs a Gulp task to watch changes in ./src/scss directory

`npm run build`
Builds a web application according to the environment variables given. This command requires `WEBAPP_COMPRESSION` and `WEBAPP_ENV` to be defined. You can find the list of all available environments in _./props/Environments.ts_ file.

`npm run server`
Launches a web server instance as per given parameters. You must provide `WEBAPP_ENV` parameter to this command. Configuration for environments is available in ./serverConfig.ts

`npm start`
Builds the application and runs the server. It should be widely used for pipeline. You must provide `WEBAPP_ENV` to this command.

`npm run start:pr`
A shortcut and default setup for PR environments (CD)

`npm run start:dev`
A shortcut and default setup for DEV environment (CD)

`npm run start:prod`
A shortcut and default setup for Production environment (CD)

`npm run test:ci-rec`
A test launcher for CI pipeline to record the tests that thrown errors and logging the test run in Cypress dashboard

`npm run test-ci`
A test launcher for CI pipeline to record the tests without recording and logging in Cypress dashboard

`npm run test:run`
A local testing launcher. Use it for text mode only if you are not interested in launching Cypress local dashboard. It does not include a web app watcher.

`npm run test:open`
A local testing launcher. Use it for Cypress UI mode.  It does not include a web app watcher.
