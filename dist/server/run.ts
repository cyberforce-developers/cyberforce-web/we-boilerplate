import WebServer from '@cyberforce/web-essentials/server'

import { Server } from 'http'
import { serverConfig, staticRoutes } from '../../src/serverConfig'

const env = process.env.WEBAPP_ENV || ''

if (env) {
  const iServer = new WebServer({
    config: serverConfig[env],
    staticRoutes
  })
  const server: Server = iServer.start()

  const sign = ['SIGINT', 'SIGTERM']
  sign.forEach((sig: any) => {
    process.on(sig, () => {
      console.info('\n\nshutting down server...\n')
      server.close()
      process.exit()
    })
  })
} else {
  throw new Error(`Unknown environment '${env}'. WEBAPP_ENV variable cannot be empty. You can choose from environments available in props/Environments.ts`)
}
