import IMasterProps from '@cyberforce/web-essentials/intf/react/IMasterProps'
import IActionFormSubmit from '@cyberforce/web-essentials/intf/actions/IActionFormSubmit'
import IActionFormValidate from '@cyberforce/web-essentials/intf/actions/IActionFormValidate'
import { IFormsState } from '../redux/enhancers/forms/state'

export default interface IPortalFormProps extends IMasterProps {
  forms: IFormsState
  onInputChange(event: any): any
  onValidate(args: IActionFormValidate): any
  onSubmit(args: IActionFormSubmit): any
}
