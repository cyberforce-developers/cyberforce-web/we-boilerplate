import * as React from 'react'
import SignUpForm from '../forms/SignUpForm'
import IMasterProps from '@cyberforce/web-essentials/intf/react/IMasterProps'

class SignUp extends React.Component<IMasterProps, {}> {
  public render(): JSX.Element {
    return (
     <div className="registration-page">
        <h1 className="registration-page-title">Registration Page</h1>
          <SignUpForm {...this.props} />
      </div>
      )
    }
  }

export default SignUp
