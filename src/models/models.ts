import IKeyAny from '@cyberforce/web-essentials/intf/IKeyAny'
import registration from './input/registration'
import signup from './input/signup'

export const inputModelsList: IKeyAny = {
  registration,
  signup
}
