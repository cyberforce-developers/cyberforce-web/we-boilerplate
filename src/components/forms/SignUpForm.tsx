import * as React from 'react'
import { connect } from 'react-redux'
import IActionFormSubmit from '@cyberforce/web-essentials/intf/actions/IActionFormSubmit'
import IActionFormValidate from '@cyberforce/web-essentials/intf/actions/IActionFormValidate'
import ValidationSummary from '@cyberforce/web-essentials/modules/validation/summary'
import RequestMethods from '@cyberforce/web-essentials/props/RequestMethods'
import InputModel from '@cyberforce/web-essentials/models/input'

import IPortalFormProps from '../../intf/IPortalFormProps'
import SignupInput from '../../models/input/signup'
import * as formsActions from '../../redux/enhancers/forms/actions'

class SignUpForm extends React.Component<IPortalFormProps, {}> {

  public componentDidUpdate(): void {
    const signup: SignupInput = this.props.forms.signup
    if (signup.canSubmit()) {
      this.submit(signup)
    }
  }

  private submit(model: InputModel): void {
    this.props.onSubmit({
      form: 'signup',
      request: {
        url: '/profile/register',
        method: RequestMethods.POST,
        payload: model.forRequest()
      }
    })
  }

  render(): JSX.Element {
    const model: SignupInput = this.props.forms.signup

    const summaryList = (model.isValidationPerformed() && !model.isValid())
      ? new ValidationSummary(model.getValidation(), this.props.locale).getList()
      : undefined

    let status
    if (model.isRequestTriggered()) {
      status = 'request triggered'
    } else if (model.isRequestCompleted()) {
      status = 'request completed'
    } else if (model.isValidationPerformed()) {
      status = 'validation performed'
    } else {
      status = ''
    }

    return (
      <div id="RegistrationForm" className="registration-form-holder">
        <h1>Registration Form</h1>
        {model.isRequestTriggered() ? <div className="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div> : <div className="registration-form-input">
          <input id='forenameInput' className={!model.isFieldValid('forename') ? "input error" : "input"} placeholder='First Name...' type="text" name="forename" value={model.getValue('forename')} onChange={this.props.onInputChange} />
          <input id='surnameInput' className={!model.isFieldValid('surname') ? 'input error' : 'input'} placeholder='Last Name...' type="text" name="surname" value={model.getValue('surname')} onChange={this.props.onInputChange} />
          <input id='ageInput' className={!model.isFieldValid('age') ? 'input error' : 'input'} placeholder='Age...' type="number" name="age" value={model.getValue('age')} onChange={this.props.onInputChange} />
          <input id='emailInput' className={!model.isFieldValid('email') ? 'input error' : 'input'} placeholder='Email Address...' type="email" name="email" value={model.getValue('email')} onChange={this.props.onInputChange} />
          <input id='passwordInput' className={!model.isFieldValid('password') ? 'input error' : 'input'} placeholder='Password...' type="password" name="password" value={model.getValue('password')} onChange={this.props.onInputChange} />
          <input id='confirmPasswordInput' className={!model.isFieldValid('password2') ? 'input error' : 'input'} placeholder='Confirm Password...' type="password" name="password2" value={model.getValue('password2')} onChange={this.props.onInputChange} />
          <button id='submitButton' onClick={() => this.props.onValidate({ key: 'signup', fields: [] })} disabled={model.getValidation().valid ? true : false}>Submit</button>
          {!model.isIdle() && <h2 id='status'>Status: {status}</h2>}
          {model.isRequestCompleted() && <h2 id='successMessage'> Email: {model.getResponse().data.email} has been created</h2>}
          {summaryList && summaryList.map(error => {
            return (
              <h2 id='error'>{error}</h2>
            )
          })}
        </div>
        }
      </div>
    );
  }
}

const stateToProps = (state: any) => {
  return {
    forms: { ...state.r_forms },
  }
}

const dispatchToProps = (dispatch: any) => {
  return {
    onInputChange: (event: any) => dispatch(formsActions.onInputChange({ key: 'signup', event })),
    onValidate: (args: IActionFormValidate) => dispatch(formsActions.onValidate(args)),
    onSubmit: (args: IActionFormSubmit) => dispatch(formsActions.onSubmit(args))
  }
}


export default connect(stateToProps, dispatchToProps)(SignUpForm)

