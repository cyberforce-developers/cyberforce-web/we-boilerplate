import * as React from 'react'

class DefaulLayout extends React.Component<any, any> {
  
  render(): JSX.Element {
    return (
      <div className="DefaultLayout">
        <h1>Layout header</h1>
        {this.props.children}
      </div>
    )
  }
}

export default DefaulLayout
