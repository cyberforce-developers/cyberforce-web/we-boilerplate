import IRoute from '@cyberforce/web-essentials/intf/IRoute'
import Index from './components/Index'
import About from './components/About'
import RegistrationPage from './components/pages/SignUp'
import Layouts from './props/Layouts'

const routes: IRoute[] = [
  {
    name: 'index',
    route: '/',
    layout: Layouts.default,
    description: 'The landing page',
    component: Index,
    exact: true
  },
  {
    name: 'about',
    route: '/about',
    description: 'About us page',
    component: About,
    exact: true
  },
  {
    name: 'register',
    route: '/register',
    description: 'Registration Page with form',
    component: RegistrationPage,
    exact: true
  }
]

export const find = (name: string): string | undefined => {
  const route = routes.find((item: IRoute) => item.name === name)
  return (route)
    ? route.name
    : undefined
}



export default routes
