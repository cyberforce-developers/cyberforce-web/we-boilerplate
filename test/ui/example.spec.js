import * as a from '../support/agent'
import { Selector } from 'testcafe'

const x = require('../../TEMPwait')

fixture `Index`
  .page `${a.project.url}`

test('check if layout exists', async t => {
  const layoutClass = Selector('.DefaultLayout').exists
  await t
    .expect(layoutClass).ok()
})

test('check if button exists', async t => {
  const button = Selector('button#changeValue').exists
  await t
    .expect(button).ok()
})


test('check if state changes after button is clicked', async t => {
  const nameSpan = Selector('#name')
  await t
    .click('button#changeValue')
      .expect(nameSpan.textContent).match(/[A-F{8}]/)
})