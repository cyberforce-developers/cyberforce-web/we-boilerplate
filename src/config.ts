import Environments from '@cyberforce/web-essentials/props/Environments'
import ICustomConfig from './intf/ICustomConfig'
import IWebAppEnvironment from '@cyberforce/web-essentials/intf/IWebAppEnvironment'

export const config: ICustomConfig = {
  environment: process.env.WEBAPP_ENV || undefined,
  envs: {
    local: {
      name: Environments.local,
      description: 'Local webapp environment',
      api: {
        url: 'http://localhost:3100',
        timeout: 3000
      }
    },
    pr: {
      name: Environments.pr,
      description: 'PR environment for the web application',
      api: {
        url: 'https://dev.core.intouchnetworks.com',
        timeout: 20000
      }
    },
    dev: {
      name: Environments.dev,
      description: 'DEV environment for the web application',
      api: {
        url: 'https://dev.core.intouchnetworks.com',
        timeout: 3000
      }
    },
    production: {
      name: Environments.production,
      description: 'Production environment for the web application',
      api: {
        url: 'https://core.intouchnetworks.com',
        timeout: 20000
      }
    }
  }
}

export const current: IWebAppEnvironment = (config.environment && config.envs.hasOwnProperty(config.environment))
  ? config.envs[config.environment]
  : config.envs.local

if (!config.environment) {
  throw new Error('No global environment defined. Check the webpack configuration and execution parametrers')
}
