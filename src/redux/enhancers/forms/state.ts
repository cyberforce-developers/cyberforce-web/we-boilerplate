import { FormActions } from '../../actions'
import * as _ from 'lodash'

import InputModel from '@cyberforce/web-essentials/models/input'
import IActionFormInputChange from '@cyberforce/web-essentials/intf/actions/IActionFormInputChange'

import LifeCycles from '@cyberforce/web-essentials/props/LifeCycles'
import RegistrationInput from '../../../models/input/registration'
import SignupInput from '../../../models/input/signup'

import middleware from '../../reducersMiddleware'
import { inputModelsList } from '../../../models/models'
import validationSchema from '../../../validationSchema'

export interface IFormsState {
  [form_name: string]: InputModel
}

const registrationModel = new RegistrationInput()
registrationModel.setSchema(validationSchema)

const signupModel = new SignupInput()
signupModel.setSchema(validationSchema)

const initialState: IFormsState = {
  registration: registrationModel,
  signup: signupModel
}

const getModelInstance = (state: IFormsState, form: string | undefined): InputModel => {
  if (state && form && inputModelsList.hasOwnProperty(form)) {
    if (state.hasOwnProperty(form) && state[form] instanceof InputModel) {
      // Model is already initialised and registered.
      return state[form]
    } else {
      // Model isn't initialised
      const ModelClass = inputModelsList[form]
      const cl: InputModel = new ModelClass()
      return cl
    }
  } else {
    throw new Error(`Model ${form} is not registered in the application`)
  }
}

const reducer = (state: IFormsState = initialState, action: any) => {
  const form = action.key // every action must have `form` property
  const newState = _.cloneDeep(state)
  switch (action.type) {
    case FormActions.OnInputChange: {
      const { event }: IActionFormInputChange = action
      const { name, value }: { name: string, value: string } = event.target || { name: undefined, value: '' }
      const model = getModelInstance(state, form)
      model.setValue(name, value)
      newState[form] = model
      return newState
    }

    case FormActions.OnValidate: {
      const { fields } = action
      const model = getModelInstance(state, form)
      model.validate(fields)
      newState[form] = model
      return newState
    }

    case FormActions.trigger: {
      const model = getModelInstance(state, form)
      model.setLifeCycle(LifeCycles.triggered)
      newState[form] = model
      return newState
    }

    case FormActions.complete: {
      const model = getModelInstance(state, form)
      model.setLifeCycle(LifeCycles.completed)
      model.setResponse(action.data)
      newState[form] = model
      return newState
    }

    case FormActions.serverError: {
      const model = getModelInstance(state, form)
      model.setLifeCycle(LifeCycles.completed)
      model.setResponse(action.error)
      newState[form] = model
      return newState
    }

    default:
      return newState
  }
}

export default middleware(reducer)

