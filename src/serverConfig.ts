import IWebServerConfig from '@cyberforce/web-essentials/intf/IWebServerConfig'
import Environments from '@cyberforce/web-essentials/props/Environments'
import IStaticRoutes from '@cyberforce/web-essentials/intf/IStaticRoutes'

// Keys should reflect Environments file
export const serverConfig: IWebServerConfig = {
  local: {
    name: Environments.local,
    description: 'Local environment',
    appDir: 'app-dev',
    port: 3000,
    compression: false
  },
  pr: {
    name: Environments.pr,
    description: 'Dynamic Pull Request environments',
    appDir: 'app-dev',
    port: 3000,
    compression: true
  },
  dev: {
    name: Environments.dev,
    description: 'Master branch',
    appDir: 'app-dev',
    port: 3000,
    compression: true
  },
  production: {
    name: Environments.production,
    description: 'Production environment',
    appDir: 'app-prod',
    port: 3000,
    compression: true
  },
  testing: {
    name: Environments.testing,
    description: 'CI testing environment',
    appDir: 'app-dev',
    port: 3000, // 8888
    compression: true
  }
}

export const staticRoutes: IStaticRoutes = {
  images: {
    uri: '/images',
    directory: '/public/images'
  },
  styles: {
    uri: '/styles',
    directory: '/public/styles'
  }
}

