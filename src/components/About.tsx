import * as React from 'react'
import { connect } from 'react-redux'

// import * as aboutActions from '../redux/about/actions' // include specific reducer actions if you want/need
import * as formsActions from '../redux/enhancers/forms/actions' // form enhancer functions
import RegistrationInput from '../models/input/registration'
import { IFormsState } from '../redux/enhancers/forms/state'
import IPortalFormProps from '../intf/IPortalFormProps'

import RequestMethods from '@cyberforce/web-essentials/props/RequestMethods'
import IActionFormSubmit from '@cyberforce/web-essentials/intf/actions/IActionFormSubmit'
import ValidationSummary from '@cyberforce/web-essentials/modules/validation/summary'
import IActionFormValidate from '@cyberforce/web-essentials/intf/actions/IActionFormValidate'
import InputModel from '@cyberforce/web-essentials/models/input'

class About extends React.Component<IPortalFormProps, IFormsState> { // IFormsState & IAboutState

  public componentDidUpdate(): void {
    const registration: RegistrationInput = this.props.forms.registration
    if (registration.canSubmit()) {
      this.submit(registration)
    }
  } 

  public render(): JSX.Element {
    const model: RegistrationInput = this.props.forms.registration

    const summary = (model.isValidationPerformed() && !model.isValid())
      ? new ValidationSummary(model.getValidation(), this.props.locale)
      : undefined

    let status
    if (model.isRequestTriggered()) {
      status = 'request triggered'
    } else if (model.isRequestCompleted()) {
      status = 'request completed'
    } else if (model.isValidationPerformed()) {
      status = 'validation performed'
    } else {
      status = 'idle'
    }

    return (
      <div>
        <h1>About page</h1>
        <h2>Back <a href="/">home</a></h2>
        <input type="text" name="name" value={model.getValue('name')} onChange={this.props.onInputChange} />
        <input type="text" name="surname" value={model.getValue('surname')} onChange={this.props.onInputChange} />
        <input type="text" name="code" value={model.getValue('code')} onChange={this.props.onInputChange} />
        <button name="submit" onClick={() => this.props.onValidate({ key: 'registration', fields: [] })}>Submit</button>
        {summary ? <h3>Summary: {summary.getList()}</h3> : undefined}
        <h3>Status: {status}</h3>
        <h2>API response after submission: {JSON.stringify(model.getResponse())} </h2>
      </div>
    )
  }

  private submit(model: InputModel): void {
    this.props.onSubmit({
      form: 'registration',
      request: {
        url: '/profile/add',
        method: RequestMethods.POST,
        payload: model.forRequest()
      }
    })
  }
  
}

// For multiple forms you can define a different action:
//  onInputChange: (form: string, event: any) => dispatch(formsActions.onInputChange({ form, event }))
// and then call it: onChange={(event: any) => this.props.onInputChange('registration', event)}


const stateToProps = (state: any) => {
  return {
    forms: {...state.r_forms},
  }
}

const dispatchToProps = (dispatch: any) => {
  return {
    onInputChange: (event: any) => dispatch(formsActions.onInputChange({ key: 'registration', event })),
    onValidate: (args: IActionFormValidate) => dispatch(formsActions.onValidate(args)),
    onSubmit: (args: IActionFormSubmit) =>  dispatch(formsActions.onSubmit(args))
  }
}


export default connect(stateToProps, dispatchToProps)(About)
