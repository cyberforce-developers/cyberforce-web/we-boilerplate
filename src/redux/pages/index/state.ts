import actions from '../../actions'
import * as _ from 'lodash'
import middleware from '../../reducersMiddleware'

import IActionStoreValue from '@cyberforce/web-essentials/intf/actions/IActionStoreValue'

export interface IIndexState {
  name: string | undefined
}

const initialState: IIndexState = {
  name: undefined
}

const reducer = (state: IIndexState, action: any) => {
  if (!state) {
    state = initialState
  }

  const newState = _.cloneDeep(state)

  switch (action.type) {
    case actions.StoreValue: {
      const { value }: IActionStoreValue = action
      newState.name = value
      return newState
    }

    default: 
      return initialState
  }
}

export default middleware(reducer)