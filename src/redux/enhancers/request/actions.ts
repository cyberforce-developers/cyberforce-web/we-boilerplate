import { RequestActions } from '../../actions'
import { request } from '../../actionsMiddleware'

import IActionTrigger from '@cyberforce/web-essentials/intf/actions/IActionTrigger'
import IActionError from '@cyberforce/web-essentials/intf/actions/IActionError'
import IActionStoreResponse from '@cyberforce/web-essentials/intf/actions/IActionStoreResponse'
import { IWebRequestSend } from '@cyberforce/web-essentials/intf/modules/IWebRequest'
import IKeyAny from '@cyberforce/web-essentials/intf/IKeyAny'
import IRequestError from '@cyberforce/web-essentials/intf/IRequestError'


export const onSend = (key: string, req: IWebRequestSend) => {
  return (dispatch: any) => {
    request(dispatch, req, {
      onSuccess: store,
      onError: serverError,
      onTrigger: trigger,
      key
    })
  }
}

const store = (params: IKeyAny, data: any): IActionStoreResponse => {
  return {
    type: RequestActions.storeResponse,
    key: params.key,
    data
  }
}

const trigger = (key: string): IActionTrigger => {
  return {
    type: RequestActions.trigger,
    key
  }
}

const serverError = (key: string, error: IRequestError): IActionError => {
  return {
    type: RequestActions.error,
    key,
    error
  }
}
