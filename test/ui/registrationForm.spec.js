import * as br from '../support/browser'
import { Selector } from 'testcafe'

fixture`Registration`
  .page`${br.project.url}/register`

const registrationInputs = {
  'forename': (Selector('#forenameInput')),
  'surname': (Selector('#surnameInput')),
  'age': (Selector('#ageInput')),
  'email': (Selector('#emailInput')),
  'password': (Selector('#passwordInput')),
  'confirmPassword': (Selector('#confirmPasswordInput')),
  'submitButton': (Selector('#submitButton')),
  'error': (Selector('#error')),
  'status': (Selector('#status')),
}

test('check if submit button exists', async t => {
  const button = Selector('button#submitButton').exists
  await t
    .expect(button).ok()
})

test('check if registration surname input exists', async t => {
  const surnameInput = Selector('input#surnameInput').exists
  await t
    .expect(surnameInput).ok()
})

test('check if registration age input exists', async t => {
  const ageInput = Selector('input#ageInput').exists
  await t
    .expect(ageInput).ok()
})

test('check if registration email input exists', async t => {
  const emailInput = Selector('input#emailInput').exists
  await t
    .expect(emailInput).ok()
})

test('check if registration password input exists', async t => {
  const passwordInput = Selector('input#passwordInput').exists
  await t
    .expect(passwordInput).ok()
})

test('check if registration confirmPassword input exists', async t => {
  const confirmPasswordInput = Selector('input#confirmPasswordInput').exists
  await t
    .expect(confirmPasswordInput).ok()
})

test('Should give error when form is submitted and validation failed ', async t => {
  await t
    .typeText(registrationInputs.forename, 'Nick')
    .typeText(registrationInputs.surname, 'Dev')
    .typeText(registrationInputs.age, '34')
    .typeText(registrationInputs.email, 'Nick@dev.com')
    .typeText(registrationInputs.password, 'Password123!')
    .typeText(registrationInputs.confirmPassword, 'thisPasswordWontMatch')
    .click(registrationInputs.submitButton)
    .expect(registrationInputs.error.innerText).eql('Password Confirmation: must match');
});

test('should successfully submit the form and pass validation', async t => {
  await t
    .typeText(registrationInputs.forename, 'Dave')
    .typeText(registrationInputs.surname, 'Dev')
    .typeText(registrationInputs.age, '34')
    .typeText(registrationInputs.email, 'dave@dev.com')
    .typeText(registrationInputs.password, 'Password123!')
    .typeText(registrationInputs.confirmPassword, 'Password123!')
    .click(registrationInputs.submitButton)
    .expect(Selector('#successMessage').innerText).eql('Email: dave@dev.com has been created');
});

test('status should not exist until the form has been submitted', async t => {
  const status = Selector('#status').exists
  await t
    .expect(status).notOk()
    .click(registrationInputs.submitButton)
    .expect(registrationInputs.status.innerText).eql('Status: validation performed');
})

test('status should equal "validation performed" when form is submitted but validation failed', async t => {
  await t
    .typeText(registrationInputs.forename, 'Dave')
    .click(registrationInputs.submitButton)
    .expect(registrationInputs.status.innerText).eql('Status: validation performed');
})

test('status should equal "request completed" when form is submitted and validation passed', async t => {
  const status = registrationInputs.status
  await t
    .typeText(registrationInputs.forename, 'Dave')
    .typeText(registrationInputs.surname, 'Dev')
    .typeText(registrationInputs.age, '34')
    .typeText(registrationInputs.email, 'dave@dev.com')
    .typeText(registrationInputs.password, 'Password123!')
    .typeText(registrationInputs.confirmPassword, 'Password123!')
    .click(registrationInputs.submitButton)
    .expect(status.innerText).eql('Status: request completed');
})

test('should fail validation when form it submitted due to confirm password input being empty', async t => {
  await t
    .typeText(registrationInputs.forename, 'Dave')
    .typeText(registrationInputs.surname, 'Dev')
    .typeText(registrationInputs.age, '34')
    .typeText(registrationInputs.email, 'dave@dev.com')
    .typeText(registrationInputs.password, 'Password123!')
    .click(registrationInputs.submitButton)
    .expect(registrationInputs.error.innerText).eql('Password Confirmation: too short: given 0, expected 8');
});

test('Should give error message "Password Confirmation: must match" when form is submitted and passwords dont match', async t => {
  await t
    .typeText(registrationInputs.forename, 'Nick')
    .typeText(registrationInputs.surname, 'Dev')
    .typeText(registrationInputs.age, '34')
    .typeText(registrationInputs.email, 'Nick@dev.com')
    .typeText(registrationInputs.password, 'Password123!')
    .typeText(registrationInputs.confirmPassword, 'thisPasswordWontMatch')
    .click(registrationInputs.submitButton)
    .expect(registrationInputs.error.innerText).eql('Password Confirmation: must match');
});

test('should fail on form validation due to user age', async t => {
  await t
    .typeText(registrationInputs.forename, 'Nick')
    .typeText(registrationInputs.surname, 'Dev')
    .typeText(registrationInputs.age, '1000')
    .typeText(registrationInputs.email, 'Nick@dev.com')
    .typeText(registrationInputs.password, 'Password123!')
    .typeText(registrationInputs.confirmPassword, 'Password123!')
    .click(registrationInputs.submitButton)
    .expect(registrationInputs.error.innerText).eql('age: too long: given 4, expected: 3')
});

test('Should give error message "email: does not match to the pattern" when form is submitted with invalid email', async t => {
  await t
    .typeText(registrationInputs.forename, 'Nick')
    .typeText(registrationInputs.surname, 'Dev')
    .typeText(registrationInputs.age, '34')
    .typeText(registrationInputs.email, 'thisEmailIsWrong.com')
    .typeText(registrationInputs.password, 'Password123!')
    .typeText(registrationInputs.confirmPassword, 'Password123!')
    .click(registrationInputs.submitButton)
    .expect(registrationInputs.error.innerText).eql('email: does not match to the pattern');
});

test('Should give error message "Password: Password must contain at least one lowercase, one uppercase, one number and one symbol" when form is submitted and password has no symbols in', async t => {
  await t
    .typeText(registrationInputs.forename, 'Nick')
    .typeText(registrationInputs.surname, 'Dev')
    .typeText(registrationInputs.age, '34')
    .typeText(registrationInputs.email, 'dev@yahoo.com')
    .typeText(registrationInputs.password, 'PasswordHasNoSymbols123')
    .typeText(registrationInputs.confirmPassword, 'PasswordHasNoSymbols123')
    .click(registrationInputs.submitButton)
    .expect(registrationInputs.error.innerText).eql('Password: Password must contain at least one lowercase, one uppercase, one number and one symbol')
});

test('should give an error message for an invalid first name', async t => {
  await t
    .typeText(registrationInputs.forename, 'N')
    .typeText(registrationInputs.surname, 'Dev')
    .typeText(registrationInputs.age, '34')
    .typeText(registrationInputs.email, 'correct@email.com')
    .typeText(registrationInputs.password, 'Password123!')
    .typeText(registrationInputs.confirmPassword, 'Password123!')
    .click(registrationInputs.submitButton)
    .expect(registrationInputs.error.innerText).eql('forename: too short: given 1, expected 3')
});

test('should give an error message for an invalid last name', async t => {
  await t
    .typeText(registrationInputs.forename, 'Developer')
    .typeText(registrationInputs.surname, 'D')
    .typeText(registrationInputs.age, '99')
    .typeText(registrationInputs.email, 'correct@email.com')
    .typeText(registrationInputs.password, 'Password123!')
    .typeText(registrationInputs.confirmPassword, 'Password123!')
    .click(registrationInputs.submitButton)
    .expect(registrationInputs.error.innerText).eql('Surname: too short: given 1, expected 2');
});
