import { RequestActions as actions } from '../../actions'
import * as _ from 'lodash'
import middleware from '../../reducersMiddleware'

import IRequestLifeCycle from '@cyberforce/web-essentials/intf/IRequestLifeCycle'
import LifeCycles from '@cyberforce/web-essentials/props/LifeCycles'
import IActionStoreResponse from '@cyberforce/web-essentials/intf/actions/IActionStoreResponse'
import IActionError from '@cyberforce/web-essentials/intf/actions/IActionError'
import IActionTrigger from '@cyberforce/web-essentials/intf/actions/IActionTrigger'

export interface IRequestState {
  [key: string]: IRequestLifeCycle
}

const initialState: IRequestState = {}

const reducer = (state: IRequestState, action: any) => {
  if (!state) {
    state = initialState
  }

  const newState = _.cloneDeep(state)

  switch (action.type) {

    case actions.trigger: {
      const { key }: IActionTrigger = action
      newState[key] = {
        lifecycle: LifeCycles.triggered,
        data: {}
      }
      return newState
    }

    case actions.storeResponse: {
      const { key, data }: IActionStoreResponse = action
      newState[key] = {
        lifecycle: LifeCycles.completed,
        data
      }
      return newState
    }

    case actions.error: {
      const { key, error }: IActionError = action
      newState[key] = {
        lifecycle: LifeCycles.completed,
        data: {},
        status: error.status
      }
      return newState
    }

    default: 
      return initialState
  }
}

export default middleware(reducer)