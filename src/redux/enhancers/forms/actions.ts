import IActionFormInputChange from '@cyberforce/web-essentials/intf/actions/IActionFormInputChange'
import IActionFormSubmit from '@cyberforce/web-essentials/intf/actions/IActionFormSubmit'
import IActionFormValidate from '@cyberforce/web-essentials/intf/actions/IActionFormValidate'
import IActionStoreResponse from '@cyberforce/web-essentials/intf/actions/IActionStoreResponse'
import IActionTrigger from '@cyberforce/web-essentials/intf/actions/IActionTrigger'
import IActionError from '@cyberforce/web-essentials/intf/actions/IActionError'
import IKeyAny from '@cyberforce/web-essentials/intf/IKeyAny'
import IRequestError from '@cyberforce/web-essentials/intf/IRequestError'

import { FormActions } from '../../actions'
import { request } from '../../actionsMiddleware'



export const onInputChange = ({ key, event }: IActionFormInputChange) => {
  return (dispatch: any) => {
    dispatch({
      type: FormActions.OnInputChange,
      key,
      event
    })
  }
}

export const onValidate = (args: IActionFormValidate) => {
  return (dispatch: any) => {
    dispatch({
      type: FormActions.OnValidate,
      key: args.key,
      fields: args.fields ? args.fields : []
    })
  }
}

export const onSubmit = (args: IActionFormSubmit) => {
  return (dispatch: any) => {
    request(dispatch, args.request, {
      onTrigger: trigger,
      onSuccess: complete,
      onError: serverError,
      key: args.form
    })
  }
}

const complete = (params: IKeyAny, data: any): IActionStoreResponse => {
  return {
    type: FormActions.complete,
    key: params.key,
    data
  }
}

const trigger = (key: string): IActionTrigger => {
  return {
    type: FormActions.trigger,
    key
  }
}

const serverError = (key: string, error: IRequestError): IActionError => {
  return {
    type: FormActions.serverError,
    key,
    error
  }
}


