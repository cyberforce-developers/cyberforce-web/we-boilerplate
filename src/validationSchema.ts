import * as deepmerge from 'deepmerge'
import * as patterns from '@cyberforce/web-essentials/props/ValidationPatterns'
import IValidationSchema from '@cyberforce/web-essentials/intf/IValidationSchema'

const RealDataSchema: IValidationSchema = {
  user_firstname: {
    min: 3,
    max: 32,
    ...patterns.onlyLetters
  },
  user_lastname: {
    min: 2,
    max: 48,
    ...patterns.onlyLetters
  },
  age: {
    min: 1,
    max: 3,
    ...patterns.onlyNumbers
  },
  email: {
    min: 1,
    max: 200,
    ...patterns.email
  },
  password: {
    min: 8,
    max: 48,
    ...patterns.password
  },
  passwordCheck: {
    min: 8,
    mustMatch: ['*password']
  }
}

const VirtualDataSchema: IValidationSchema = {
  code: {
    max: 8,
    ...patterns.onlyNumbers
  }
}

export default deepmerge(RealDataSchema, VirtualDataSchema)
