import InputModel from '@cyberforce/web-essentials/models/input'

export default class RegistrationInput extends InputModel {
  constructor() {
    super({
      name: 'registration', 
      data: {
        name: {
          _: '',
          v: 'user_firstname'
        },
        surname: {
          _: '',
          v: 'user_lastname'
        },
        code: {
          _: '',
          v: 'code',
          exact: true
        }
      }
    })
  }

}
