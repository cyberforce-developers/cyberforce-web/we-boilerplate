import IWebAppConfig from '@cyberforce/web-essentials/intf/IWebAppConfig'

export default interface ICustomConfig extends IWebAppConfig {
  environment: string | undefined
}
