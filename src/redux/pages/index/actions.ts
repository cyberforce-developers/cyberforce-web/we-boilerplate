import actions from '../../actions'

import IActionStoreValue from '@cyberforce/web-essentials/intf/actions/IActionStoreValue'

export const setValue = (name: string) => {
  return (dispatch: any) => {
    dispatch(storeValue(name))
  }
}

const storeValue = (value: string): IActionStoreValue => {
  return {
    type: actions.StoreValue,
    value
  }
}

