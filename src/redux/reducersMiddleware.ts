const bind = require('minified-headless').bind

export default (reducer: any) => {

  // Add your middleware here. You can use it in all reducers

  return bind(reducer, null, null,
    {}  // Link your middleware
  )
}