import { current } from '../config'
import { AxiosResponse } from 'axios'
import { IWebRequestSend } from '@cyberforce/web-essentials/intf/modules/IWebRequest'
import IActionRequestParams from '@cyberforce/web-essentials/intf/actions/IActionRequestParams'
import IRequestError from '@cyberforce/web-essentials/intf/IRequestError'

import WebRequest from '@cyberforce/web-essentials/modules/request/web'

export const request = (dispatch: any, req: IWebRequestSend, params: IActionRequestParams) => {
  const onSuccess = params.onSuccess
  const onError = params.onError
  const onTrigger = params.onTrigger
  delete params.onSuccess
  delete params.onError
  delete params.onTrigger
  dispatch(onTrigger(params.key))
  new WebRequest({
    baseUrl: current.api.url,
    timeout: current.api.timeout
  }).send(req)
    .then((r: AxiosResponse) => {
      if (r.status === 200) {
        dispatch(onSuccess({...params}, r.data))
      } else {
        dispatch(onError(params.key, {
          status: r.status,
          data: r.data
        }))
      }
    }).catch((error: any) => {
      const err: IRequestError = error.response
        ?
          {
            status: error.response.status || 500,
            statusText: error.response.statusText || '?',
            data: error.response.data || {}
          }
        : {
            status: 500,
            statusText: error,
            data: {}
          }
      console.warn(`Request error. ${req.url}`)
      dispatch(onError(params.key, err))
    })
}