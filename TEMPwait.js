const waitPort = require('wait-port')
const params = {
  host: 'localhost',
  port: 8888,
  timeout: 10000
}

module.exports = function() {
  waitPort(params)
  .then((open) => {
    console.log(open
      ? 'The port is now open!'
      : 'The port did not open before the timeout...'
    )
  })
  .catch((err) => {
    console.err(`An unknown error occured while waiting for the port: ${err}`);
  })
}