import DefaulLayout from '../components/layouts/Default'
import IKeyAny from '@cyberforce/web-essentials/intf/IKeyAny'

const Layouts: IKeyAny = {
  default: DefaulLayout
}

export default Layouts
