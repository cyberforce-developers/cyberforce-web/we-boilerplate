const t = {
  title: "English translation",
  inputs: {
    name: {
      label: 'Name',
      validation: 'custom validation text for name for regex'
    },
    surname: {
      label: 'Surname'
    },
    code: {
      label: 'Code'
    },
    password: {
      label: 'Password',
      validation: 'Password must contain at least one lowercase, one uppercase, one number and one symbol'
    },
    password2: {
      label: 'Password Confirmation',
      validation: 'Passwords do not match'
    }
  },
  validation: {
    unknown: 'unknown error',
    given: 'given',
    expected: 'expected',
    tooLong: 'too long',
    tooShort: 'too short',
    mustMatch: 'must match',
    mustNotMatch: 'must not match',
    regex: 'does not match to the pattern',
    eitherOr: 'either one of the values need to be good'
  }
}

module.exports = t